import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Drawer, Divider } from '@material-ui/core';

import NavigationButton from './NavigationButton'
import logo from '../Images/logo.svg'

const drawerWidth = 240;

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    width: drawerWidth,
  },
  toolbar: theme.mixins.toolbar,
  sublist: {
    padding: '20px'
  },
  logo: {
    padding: '15px',
    width: '80%',
    heigth: 'auto'
  },
  title: {
    fontFamily: 'note-tracker-icon-font',
  }
});

class NavigationBar extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      anchor: 'left',
    };
  }

  handleChange(event) {
    this.setState({
      anchor: event.target.value,
    });
  };

  render() {
    const { classes } = this.props;

    const drawer = (
      <Drawer
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <img src={logo} className={classes.logo} alt='logo' />
        <div className={classes.toolbar} />
        <NavigationButton
          variant='headline'
          text='All notes'
          link='notes/'
          getItems={this.props.getItems}
        />
        <div className={classes.sublist}>
          <NavigationButton
            variant='title'
            text='Notes'
            link='notes?type=text'
            getItems={this.props.getItems}
          />
          <NavigationButton
            variant='title'
            text='Images'
            link='notes?type=image'
            getItems={this.props.getItems}
          />
          <NavigationButton
            variant='title'
            text='Links'
            link='notes?type=link'
            getItems={this.props.getItems}
          />
        </div>
        <Divider />
        <NavigationButton
            variant='headline'
            text='Trash'
            link='trash/'
            getItems={this.props.getItems}
          />
      </Drawer>
    )

    return (
      <div className={classes.root}>
        {drawer}
      </div>
    );
  }
}

export default withStyles(styles)(NavigationBar);
