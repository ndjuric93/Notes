# Notes

Simple client server example for keeping notes written with Django and React.

The possibility of the app is to add/remove/edit notes consisting of Text, Link and Image. After that you can view and filter notes.

## Server Side

Server side consists of Django ViewSets for Notes and Trash, as note can be either in Note and Trash.

To set up the side, install requirements from requirements.txt using `pip install -r requirements.txt` in the server directory.

After that, set up the sqlite database by running

`python manage.py make migrations`

`python manage.py migrate`

The database is ready, so you can start up the server by running

`python manage.py runserver`

## Client Side

Client side consists of a React App. It uses webpackage for deployment. To set it up, you need npm package manager. Install dependencies by running.

`npm install`

After that, there are two possible scripts for both development and production and you can run for development:

`npm run dev`

And

`npm run build`

For production.

The index.html will be generated in dist/ folder in website folder. It requires server being up and running.