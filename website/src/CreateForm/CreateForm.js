import React from 'react';

import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
} from '@material-ui/core';

import TypeSelector from './TypeSelector';

export default class CreateForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      description: 'Note',
      selected: 'text',
      title: '',
      text: '',
    };
  }

  componentDidMount () {
    let data = this.props.data;
    let selected = data.link !== '' ? 'link' : (data.image !== '' ? 'image' : 'text')
    this.setState ({
      title: data.title,
      text: data[selected],
      selected: selected,
    });

    this.setSelected(selected)
  }

  setSelected(value) {
    this.setState ({selected: value});
  }

  handleChange(name, event) {
    this.setState ({
      [name]: event.target.value,
    });
  }

  handleOpen() {
    this.setState ({[this.props.state]: true});
  };

  setInput(text, title, selected) {
    this.setState ({
      text: text,
      title: title,
      selected: selected,
    });
    this.setSelected (selected);
  };

  handleOk() {
    this.props.handlePost(
      this.state.title,
      this.state.text,
      this.state.selected
    );
    this.handleClose ();
  };

  handleClose() {
    this.setInput('', '', 'text')
    this.props.closeForm ();
  };

  changeDescription (description) {
    this.setState ({
      description: description,
    });
  }

  render() {
    return (
      <div>
        <Dialog open={this.props.openState} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">Add Note</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Choose what kind of note would you like to add.
            </DialogContentText>
            <TypeSelector
              setSelected={this.setSelected.bind(this)}
              handler={this.changeDescription.bind(this)}
            />
            <TextField
              id="title"
              label="Title"
              value={this.state.title}
              onChange={(event) => this.handleChange('title', event)}
              margin="normal"
              fullWidth
            />
            <TextField
              id="title"
              label={this.state.description}
              onChange={(event) => this.handleChange('text', event)}
              value={this.state.text}
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose.bind(this)} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleOk.bind(this)} color="primary">
              OK
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
