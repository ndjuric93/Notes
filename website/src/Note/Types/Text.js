import React from 'react';

import { Typography, withStyles } from '@material-ui/core/';

const styles = {
    title: {
        fontFamily: 'note-tracker-icon-font',
    }
};

function Text(props) {
    const { classes } = props
    return (
        <div>
            <Typography gutterBottom variant="headline" component="h2" className={classes.title}>
                {props.title}
            </Typography>
            <Typography component="p" className={classes.title}>
                {props.text}
            </Typography>
        </div>
    )
};

export default withStyles(styles)(Text)
