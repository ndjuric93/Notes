import React from 'react';

import {Typography, withStyles} from '@material-ui/core/';

const styles = {
  title: {
      fontFamily: 'note-tracker-icon-font',
  }
};

function Link(props) {
  const { classes } = props
  return (
    <div>
        <Typography gutterBottom variant="headline" component="h2" className={classes.title}>
          {props.title}
        </Typography>
        <Typography component="a" href={props.link}>
          {props.link}
        </Typography>
    </div>
  )
};

export default withStyles(styles)(Link)
