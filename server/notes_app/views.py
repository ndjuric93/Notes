""" Module for Views in Notes App """
from django.http import HttpResponse
from django.db.models import Q

from rest_framework import viewsets

from notes_app.models import TrashNote, Note
from notes_app.serializers import NoteSerializer

class NotesViewSet(viewsets.ModelViewSet):
    """ ViewSet for notes """
    serializer_class = NoteSerializer
    queryset = Note.objects.all()

    def get_queryset(self):
        """ Returns query set depending on the request. """
        queryset = Note.objects
        note_type = self.request.query_params.get('type', None)
        if note_type == 'text':
            return queryset.exclude(Q(text=''))
        if note_type == 'image':
            return queryset.exclude(Q(image=''))
        if note_type == 'link':
            return queryset.exclude(Q(link=''))
        return queryset

    def destroy(self, request, *args, **kwargs):
        """ Destroy method that moves the data to trash table.

        Parameters
        ----------
        request : dict
            Http request

        kwargs : dict
            Key value arguments

        Returns
        -------
        HttpResponse
            Http response with success message
        """
        note = Note.objects.filter(pk=kwargs['pk'])
        values = note.values()[0]
        TrashNote.objects.create(
            title=values['title'],
            text=values['text'],
            link=values['link'],
            image=values['image']
        )
        Note.objects.filter(pk=kwargs['pk']).delete()
        return HttpResponse('Deleted')


    def update(self, request, *args, **kwargs):
        """ Updates entry.

        Parameters
        ----------
        request : dict
            Http request

        pk : int
            Primary key of the field

        Returns
        -------
        HttpResponse
            HttpResponse with success message
        """
        data = request.data
        note = Note.objects.get(pk=kwargs['pk'])
        # Set all fields to '' except the one Note is actually refering to
        note.title = NotesViewSet.get_field(data, 'title')
        note.image = NotesViewSet.get_field(data, 'image')
        note.text = NotesViewSet.get_field(data, 'text')
        note.link = NotesViewSet.get_field(data, 'link')
        if 'color' in data:
            note.color = data['color']
        note.save()
        return HttpResponse('Updated')

    @staticmethod
    def get_field(data, field_type):
        """ Returns field if exists, otherwise empty string

        Parameters
        ----------
        field_type : str
            Name of field

        Returns
        -------
        str
            Data in field type, if exists, otherwise empty string
        """
        return data[field_type] if field_type in data and data[field_type] != '' else ''


class TrashViewSet(viewsets.ModelViewSet):
    '''! ViewSet for notes that should be stored in trash '''
    queryset = TrashNote.objects.all()
    serializer_class = NoteSerializer
