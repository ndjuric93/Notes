""" Module for Notes Django Application """
from django.apps import AppConfig

class NotesAppConfig(AppConfig):
    """ Django App config for Notes Application """
    name = 'notes_app'
