'''Module for serializers of models'''
from django.utils import timezone

from rest_framework import serializers

from notes_app.models import Note

class NoteSerializer(serializers.ModelSerializer):
    """
    Serializer to map the Note instance into JSON format.
    """
    created = serializers.DateTimeField(default=timezone.now())
    title = serializers.CharField()
    text = serializers.CharField(required=False)
    image = serializers.CharField(required=False)
    link = serializers.CharField(required=False)
    color = serializers.CharField(required=False)

    class Meta:
        """
        Meta class to map serializers field with the model's fields
        """
        model = Note
        fields = '__all__'
