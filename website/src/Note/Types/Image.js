import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import { CardMedia, Typography } from '@material-ui/core/';

const styles = theme => ({
    media: {
      paddingTop: '46.25%'
    },
  });

function Image(props) {
  const { classes } = props;
  return (
    <div>
      <Typography gutterBottom variant="headline" component="h2" className={classes.title}>
        {props.title}
      </Typography>
      <CardMedia
        className={classes.media}
        image={props.image}
      />
    </div>
  )
};

Image.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Image);
