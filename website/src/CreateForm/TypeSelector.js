import React from 'react';
import { Radio, RadioGroup, FormControlLabel } from '@material-ui/core/';


class TypeSelector extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      value: 'text',
    };
  }

  handleChange(event) {
    this.setState({
      value: event.target.value,
      label: event.target.label,
    });
    this.props.setSelected(event.target.value)
    this.props.handler(event.target.value)
  };

  render() {
    return (
      <div>
        <RadioGroup
          aria-label="Type"
          name="type"
          value={this.state.value}
          onChange={this.handleChange.bind(this)}
          row
        >
          <FormControlLabel value="text" control={<Radio color="primary" />} label="Note" />
          <FormControlLabel value="image" control={<Radio color="primary" />} label="Image" />
          <FormControlLabel value="link" control={<Radio color="primary" />} label="Link" />
        </RadioGroup>
      </div>
    );
  }
}

export default TypeSelector
