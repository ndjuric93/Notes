""" Module for testing of Note app """
from django.test import TestCase

from rest_framework.test import APIClient
from rest_framework.reverse import reverse

from notes_app.models import Note

class NoteTest(TestCase):
    """ Class for testing Note Model """


    def setUp(self):
        """ Set up for creating note test case """
        note = Note(text='Lorem Ipsum')
        note.save()

    def test_can_create_text_note(self):
        """ Test if note can be created """
        self.assertEqual(Note.objects.count(), 1)

class NoteApiTest(NoteTest):
    """ TestCase for testing Note api """

    def setUp(self):
        """ Set up for test case """
        super(NoteApiTest, self).setUp()
        self.client = APIClient()

    def test_create_note(self):
        """ Test for testing creating a text note """
        notes_data = {
            'title': 'Lorem',
            'text': 'Lorem Ipsum'
        }
        response = self.client.post(
            reverse('note-list'),
            notes_data,
            format='json'
        )
        self.assertEqual(response.status_code, 201)
