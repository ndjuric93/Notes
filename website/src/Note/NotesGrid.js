import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Button, Grid, GridList, GridListTile } from '@material-ui/core/';
import Note from './Note'
import Text from './Types/Text'
import Image from './Types/Image'
import Link from './Types/Link'
import axios from 'axios'

import CreateForm from '../CreateForm/CreateForm'

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper
  },
  gridList: {
    width: '100%',
    height: '100%',
    paddingTop: '10px'
  },
  media: {
    height: 0,
    paddingTop: '56.25%'
  },
});

class NotesGrid extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      'create_form_open': false,
    }
  }

  getTypeToRender(data) {
    if(data.text !== '') {
      return (
        <Text title={data.title} text={data.text} />
      )
    } else if(data.image !== '') {
      return (
        <Image image={data.image} title={data.title} />
      )
    } else {
      return (
        <Link link={data.link} title={data.title} />
      )
    }
  }

  sendPost(title, text, selected) {
    axios.post(process.env.API_URL + 'notes/', {
      title: title,
      [selected]: text
    }).then(() => {
      this.props.getItems('notes/')
    })
  }

  handleFormStatus(value) {
    this.setState({create_form_open: value})
  }

  render() {
      const { classes } = this.props;
      return (
        <div>
          <Grid container justify='flex-end'>
            <Button onClick={this.handleFormStatus.bind(this, true)} variant="contained" color="primary" >Add Note</Button>
            <CreateForm
              data={{text:'',image:'',link:''}}
              getItems={this.props.getItems}
              handlePost={this.sendPost.bind(this)}
              closeForm = {this.handleFormStatus.bind(this, false)}
              openState = {this.state.create_form_open}
            />
          </Grid>
          <div className={classes.root}>
            <GridList cellHeight={250} className={classes.gridList} cols={3} padding={20} >
              {this.props.data.map(tile => (
                <GridListTile key={tile.id}>
                  <Note
                    data={tile}
                    children={this.getTypeToRender(tile)}
                    getItems={this.props.getItems}
                  />
                </GridListTile>
              ))}
            </GridList>
          </div>
        </div>
      );
  }
}

NotesGrid.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NotesGrid);
