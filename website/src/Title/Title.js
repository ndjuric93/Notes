import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { AppBar, Toolbar, Grid,  Typography, withStyles } from '@material-ui/core';

const drawerWidth = 240

const styles = {
    appBar: {
        width: `calc(100% - ${drawerWidth}px)`,
    },
    title: {
        fontFamily: 'note-tracker-icon-font',
    }
  };

function Title(props) {
    const {classes} = props
    return (
      <div className={classes.root}>
          <AppBar
              position='absolute'
              color='inherit'
              className={classNames(classes.appBar)}
          >
              <Grid container alignItems='center' alignContent='space-between'>
                  <Toolbar>
                      <Typography variant="display1" className={classes.title}>
                          Notes
                      </Typography>
                  </Toolbar>
              </Grid>
          </AppBar>
      </div>
    )
}

Title.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Title)
