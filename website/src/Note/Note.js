import React from 'react';
import PropTypes from 'prop-types';
import {
  Grid,
  Button,
  Card,
  CardActions,
  withStyles,
  Popover,
} from '@material-ui/core/';
import { GithubPicker } from 'react-color';

import axios from 'axios';
import CreateForm from '../CreateForm/CreateForm';

const styles = {
  card: {
    maxWidth: 345,
    maxHeight: 300,
    backgroundColor: 'white'
  },
  title: {
    fontFamily: 'note-tracker-icon-font',
  }
};

class Note extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      color_picker: false,
      form_open: false,
      title: '',
      text: '',
      type: '',
      color: props.data.color
    };
  }

  handleDelete() {
    axios
      .delete (process.env.API_URL + 'notes/' + this.props.data.id)
      .then (() => this.props.getItems ('notes/'));
  };

  modifyRequest(title, text, type) {
    axios
      .put (process.env.API_URL + 'notes/' + this.props.data.id + '/', {
        title: title,
        [type]: text,
      })
      .then (() => this.props.getItems ('notes/'));
  };

  handleFormOpen(value) {
    this.setState ({form_open: value});
  };

  handlePicker(value) {
    this.setState ({color_picker: value});
  };

  handleColorChange(value) {
    let data = this.props.data
    let type = data.text != '' ? 'text' : (data.image != '' ? 'image' : 'link')
    axios
      .put (process.env.API_URL + 'notes/' + this.props.data.id + '/', {
        title: data.title,
        [type]: data.text != '' ? data.text : (data.image != '' ? data.image : data.link),
        color: value.hex
      })
      .then (() => this.props.getItems ('notes/'))
      .then(() => this.setState({color:value.hex}));
  }

  render() {
    const {classes} = this.props;

    return (
      <div>
        <CreateForm
          openState={this.state.form_open}
          closeForm={this.handleFormOpen.bind(this, false)}
          handlePost={this.modifyRequest.bind(this)}
          data={this.props.data}
        />
        <Card className={classes.card} style={{backgroundColor: this.state.color}} >
          {this.props.children}
          <CardActions>
            <Grid container justify='center' spacing={16}>
              <Grid item>
                <Button onClick={this.handleFormOpen.bind(this,true)} variant='contained' color='primary' size='small'>
                  Edit
                </Button>
              </Grid>
              <Grid item>
                <Button onClick={this.handleDelete.bind(this)} variant='contained' color='primary' size='small'>
                  Delete
                </Button>
              </Grid>
              <Grid item>
                <Button onClick={this.handlePicker.bind(this, true)} variant='contained' color='primary' size='small'>
                  Color
                </Button>
                <Popover
                    open={this.state.color_picker}
                    onClose={this.handlePicker.bind(this, false)}
                >
                  <GithubPicker onChangeComplete={ this.handleColorChange.bind(this) } />
                </Popover>
              </Grid>
            </Grid>
          </CardActions>
        </Card>
      </div>
    );
  }
}

Note.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles (styles) (Note);
