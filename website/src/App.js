import React from 'react';

import { withStyles } from '@material-ui/core/styles';

import axios from 'axios'

import NavigationBar from './Navigation/NavigationBar'
import NotesGrid from './Note/NotesGrid'

import Title from './Title/Title'

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  appFrame: {
    position: 'relative',
    display: 'flex',
    width: '100%',
    height: '100%',
  },
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    width: '100%',
    height: '100%'
  },
});

class App extends React.Component {

  constructor() {
    super()
    this.state = {
      items: [],
    }
  }

  componentDidMount() {
    this.getItems('notes/');
  }

  getItems(resource) {
    axios.get(process.env.API_URL + resource).then(response => this.setState({ items: response.data }))
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <div className={classes.appFrame}>
          <NavigationBar getItems={this.getItems.bind(this)} />
          <Title getItems={this.getItems.bind(this)} />
          <main className={classes.content}>
            <div className={classes.toolbar} />
            <NotesGrid data={this.state.items} getItems={this.getItems.bind(this)} />
          </main>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(App);
