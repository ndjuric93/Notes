""" Module for urls in the Notes application """
from django.conf.urls import url, include

from rest_framework.routers import DefaultRouter

from notes_app.views import NotesViewSet, TrashViewSet

ROUTER = DefaultRouter()
ROUTER.register(r'notes', NotesViewSet)
ROUTER.register(r'trash', TrashViewSet)

urlpatterns = [
    url(r'^', include(ROUTER.urls,))
]
