""" Module for admin in Notes App """
from django.contrib import admin

from notes_app.models import Note

# Register your models here.
admin.site.register(Note)
