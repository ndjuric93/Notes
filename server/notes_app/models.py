'''Module for Models in Notes App'''
from django.db import models

class BaseNote(models.Model):
    """
    Base model for note

    This model contains creation time, title, text, image and link
    """
    created = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=256, blank=True)
    text = models.CharField(max_length=1024, blank=True)
    image = models.CharField(max_length=1024, blank=True)
    link = models.CharField(max_length=1024, blank=True)
    color = models.CharField(max_length=16, blank=True)

    class Meta:
        abstract = True


class Note(BaseNote):
    """ Implementation of BaseNote"""
    pass

class TrashNote(BaseNote):
    """ Implementation of BaseNote"""
    pass
