import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { ListItem, ListItemText, Typography } from '@material-ui/core';

const styles = theme => ({
    title: {
        fontFamily: 'note-tracker-icon-font',
    }
});

function NavigationButton(props) {
    const { classes } = props
    return (
        <ListItem button>
            <ListItemText
                primary={<Typography variant={props.variant} className={classes.title}>{props.text}</Typography>}
                onClick={() => {props.getItems(props.link)}}
            />
        </ListItem>
    )
}

export default withStyles(styles)(NavigationButton);
